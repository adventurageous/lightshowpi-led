// Ref : http://www.esp8266.com/viewtopic.php?f=28&t=2295&p=13730#p13730

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <string.h>
#include <stdio.h>

#include <NeoPixelBus.h>
const uint16_t PixelCount = 6; // this example assumes 4 pixels, making it smaller will cause a failure
const uint8_t PixelPin = 2;  // make sure to set this to the correct pin, ignored for Esp8266
#define colorSaturation 128

const char *ssid = "SETC"; const char *password = "";
ESP8266WebServer server(80);

// three element pixels, in different order and speeds
NeoPixelBus<NeoGrbFeature, Neo800KbpsMethod> strip(PixelCount, PixelPin);
//NeoPixelBus<NeoRgbFeature, Neo400KbpsMethod> strip(PixelCount, PixelPin);

WiFiUDP port;

char packetBuffer[255];
char firstchar[1];
unsigned int localPort = 8888;
int ledData[18];
char character;
String content = "";
RgbColor color;

void handleRoot() {
  server.send(200, "text/html", "<h1>You are connected</h1>");
}

void setup() {
  Serial.begin(250000);

  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.softAP("esplightshow", "");

  IPAddress myIP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(myIP);
  server.on("/", handleRoot);
  server.begin();
  Serial.println("HTTP server started");
  
  //WiFi.begin("SETC", "");
  port.begin(localPort);
  pinMode(0, OUTPUT);

  strip.Begin();
  strip.Show();
}

void loop() {
  int packetSize = port.parsePacket();
  // Serial.println(packetSize);
  if (packetSize) {
    strncpy(firstchar, packetBuffer,1);
    int len = port.read(packetBuffer, 255);
    if (len > 0) packetBuffer[len-1] = 0;

    int i = 1;
    int arraynum = 0;
    content = "";
    while ( i < len ) {
      if ( String(packetBuffer[i]) != ",") {
        content.concat(packetBuffer[i]);
      }
      else {
        ledData[arraynum] = content.toInt();
        content ="";
        arraynum++;
        i++;
      }
      i++;
      }
    ledData[17] = content.toInt(); 

    Serial.println(packetBuffer);
    //Serial.print("ledData0 = "); Serial.println(ledData[0]); 
    //Serial.print("ledData1 = "); Serial.println(ledData[1]); 
    //Serial.print("ledData18 = "); Serial.println(ledData[18]);     
    //Serial.print("ledData17 = "); Serial.println(ledData[17]); 

    color.R = ledData[0]; // how ever you parse it
    color.G = ledData[1];
    color.B = ledData[2];
    strip.SetPixelColor(0, color);
    
    color.R = ledData[3]; // how ever you parse it
    color.G = ledData[4];
    color.B = ledData[5];
    strip.SetPixelColor(1, color);
    
    color.R = ledData[6]; // how ever you parse it
    color.G = ledData[7];
    color.B = ledData[8];
    strip.SetPixelColor(2, color);
    
    color.R = ledData[9]; // how ever you parse it
    color.G = ledData[10];
    color.B = ledData[11];
    strip.SetPixelColor(3, color);
    
    color.R = ledData[12]; // how ever you parse it
    color.G = ledData[13];
    color.B = ledData[14];
    strip.SetPixelColor(4, color);
    
    color.R = ledData[15]; // how ever you parse it
    color.G = ledData[16];
    color.B = ledData[17];
    strip.SetPixelColor(5, color);
    strip.Show();
            
  }
  // delay(500);
}
